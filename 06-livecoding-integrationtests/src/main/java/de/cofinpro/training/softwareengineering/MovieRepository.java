package de.cofinpro.training.softwareengineering;

import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

/**
 * Kümmert sich um den Datenzugriff auf {@link Movie}-Objekte.
 */
@Slf4j
public class MovieRepository {
    private final Connection connection;

    public MovieRepository(Connection connection) {
        this.connection = connection;
    }

    public void insert(Movie movie) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement("insert into movie (title) values (?)")) {
            statement.setString(1, movie.getTitle());
            statement.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            connection.rollback();
        }
    }

    public Optional<Movie> getFirstMovieStartingWith(String title) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement("select * from movie where title like ? order by insert_timestamp desc")) {
            statement.setString(1, title + "%");
            try (ResultSet resultSet = statement.executeQuery();) {
                 if (resultSet != null && resultSet.next()) {
                    return Optional.of(new Movie(resultSet.getString("title")));
                }
            }
        }

        return Optional.empty();
    }
}
