package de.cofinpro.training.softwareengineering;

/**
 * Steht für fehlende Bewertungen.
 */
public class NoRatingsException extends Exception {
}
