package de.cofinpro.training.softwareengineering;

import com.github.tomakehurst.wiremock.junit5.WireMockRuntimeInfo;
import com.github.tomakehurst.wiremock.junit5.WireMockTest;
import com.github.tomakehurst.wiremock.matching.UrlPathPattern;
import org.junit.jupiter.api.Test;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

@WireMockTest
class HttpRatingServiceIT {

    @Test
    void should_call_correct_endpoint_when_using_http_rating_service(WireMockRuntimeInfo wireMockRuntimeInfo) {
        stubFor(put(ratingEndpointUrlPattern()).willReturn(aResponse().withStatus(200)));

        RatingService ratingService = new HttpRatingService(wireMockRuntimeInfo.getHttpPort());
        ratingService.addRating(new Movie("The Big Short"), 3);

        verify(putRequestedFor(ratingEndpointUrlPattern())
                .withUrl("/movie/The%20Big%20Short/rate/3")
                .withRequestBody(absent()));
    }

    private UrlPathPattern ratingEndpointUrlPattern() {
        return urlPathMatching("/movie/.*/rate/\\d+");
    }
}