#language: de

  Funktionalität: Film
    Szenario: Berechnung einer Durchschnittsbewertung für einen einzelnen Film
      Gegeben sei der Film "The Big Short" mit einer einzelnen Bewertung von 3
      Wenn der Film eine zusätzliche Bewertung von 5 erhält
      Und anschließend die Bewertungen zurückgesetzt werden
      Und dann die Durchschnittsbewertung durchgeführt wird
      Dann wird ein Fehler der Klasse "NoRatingsException" geschmissen.