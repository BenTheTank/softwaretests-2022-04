Feature: Average Ratings of Movies
  Scenario: Calculation of average rating for a single movie
    Given movie "The Big Short" has one rating of 3
    When the movie receives a new rating of 5
    Then the average rating of the movie is 4

    Scenario: Calculation of two average ratings for two movies
