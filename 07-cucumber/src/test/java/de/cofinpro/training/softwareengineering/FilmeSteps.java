package de.cofinpro.training.softwareengineering;

import io.cucumber.java.PendingException;
import io.cucumber.java.de.Dann;
import io.cucumber.java.de.Gegebensei;
import io.cucumber.java.de.Und;
import io.cucumber.java.de.Wenn;

public class FilmeSteps {
    @Gegebensei("der Film {string} mit einer einzelnen Bewertung von {int}")
    public void derFilmMitEinerEinzelnenBewertungVon(String arg0, int arg1) {
        throw new PendingException();
    }

    @Wenn("der Film eine zusätzliche Bewertung von {int} erhält")
    public void derFilmEineZusätzlicheBewertungVonErhält(int arg0) {
        throw new PendingException();
    }

    @Und("anschließend die Bewertungen zurückgesetzt werden")
    public void anschließendDieBewertungenZurückgesetztWerden() {
        throw new PendingException();
    }

    @Und("dann die Durchschnittsbewertung durchgeführt wird")
    public void dannDieDurchschnittsbewertungDurchgeführtWird() {
        throw new PendingException();
    }

    @Dann("wird ein Fehler der Klasse {string} geschmissen.")
    public void wirdEinFehlerDerKlasseGeschmissen(String arg0) {
        throw new PendingException();
    }
}
