package de.cofinpro.training.softwareengineering;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.util.Collections.unmodifiableList;

/**
 * Repräsentiert einen Film mit seinen Bewertungen.
 */
public class Movie {
    private final String title;
    static List<Integer> ratings = new ArrayList<>();

    public Movie(String title) {
        this.title = title;
    }

    public void addRating(int rating) {
        ratings.add(rating);
    }

    public void addRatings(int... ratings) {
        Arrays.stream(ratings).forEach(this::addRating);
    }

    public List<Integer> getRatings() {
        return unmodifiableList(ratings);
    }

    public String getTitle() {
        return title;
    }

    public double getAverageRating() throws NoRatingsException {
        if (ratings.isEmpty()) {
            throw new NoRatingsException();
        }
        return ratings.stream().reduce(0, Integer::sum) / (double) ratings.size();
    }
}
