package de.cofinpro.training.softwareengineering;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Named.named;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class ParameterTest {
    @ParameterizedTest(name = "{displayName} {index} {arguments}")
    @ValueSource(ints = {1, 4, 5, 6, 7})
    @DisplayName("The display name")
    void withInts(int value) {
        assertThat(value).isEqualTo(value);
    }

    @ParameterizedTest(name = "{displayName} String-Parameter: {0}")
    @CsvSource({
            "aaa, 1",
            "bbb, 2",
            "ccc, 5"
    })
    @DisplayName("The display name")
    void withCsv(String string, int number) {
        assertThat(string).isEqualTo("bbb");
    }

    @ParameterizedTest
    @MethodSource("generateCustomObjects")
    void withCustomSource(CustomObject customObject) {
        assertThat(customObject.id).isEqualTo(775);
    }

    // Diese Methode verwendet die Naming API von JUnit 5, um nicht nur Argumente für den Test
    // zu liefern aber auch den DisplayName
    // siehe: https://junit.org/junit5/docs/current/user-guide/#writing-tests-parameterized-tests-display-names
    private static Stream<Arguments> generateCustomObjects() {
        CustomObject customObject1 = new CustomObject(155);
        CustomObject customObject2 = new CustomObject(775);

        return Stream.of(
                arguments(named(customObject1.getNameForTest(), customObject1)),
                arguments(named(customObject2.getNameForTest(), customObject2))
        );
    }

    private static class CustomObject {
        private final int id;
        private CustomObject(int id) {
            this.id = id;
        }
        private String getNameForTest() {
            return "Custom-Object with id " + id;
        }
    }
}
