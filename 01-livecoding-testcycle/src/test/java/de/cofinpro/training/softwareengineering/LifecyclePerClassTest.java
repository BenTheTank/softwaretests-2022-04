package de.cofinpro.training.softwareengineering;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;

/**
 * Entnommen von
 * <a href="https://github.com/bonigarcia/mastering-junit5/blob/master/junit5-basic-tests/src/test/java/io/github/bonigarcia/LifecycleTest.java">https://github.com/bonigarcia/mastering-junit5/blob/master/junit5-basic-tests/src/test/java/io/github/bonigarcia/LifecycleTest.java</a>
 */
@Slf4j
@TestInstance(PER_CLASS)
class LifecyclePerClassTest {

    @BeforeAll
    static void setupAll() {
        log.debug("@BeforeAll");
    }

    @BeforeEach
    void setup() {
        log.debug("@BeforeEach - {}", this);
    }

    @Test
    void test1() {
        log.debug("@Test [1] - {}", this);
    }

    @Test
    void test2() {
        log.debug("@Test [2] - {}", this);
    }

    @AfterEach
    void teardown() {
        log.debug("@AfterEach - {}", this);
    }

    @AfterAll
    static void teardownAll() {
        log.debug("@AfterAll");
    }
}
