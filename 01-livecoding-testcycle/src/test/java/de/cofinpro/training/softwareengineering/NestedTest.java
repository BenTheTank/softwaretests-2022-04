package de.cofinpro.training.softwareengineering;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Slf4j
class NestedTest {
    public NestedTest() {
        log.debug("Constructor");
    }

    @BeforeAll
    static void setupAll() {
        log.debug("@BeforeAll");
    }

    @BeforeEach
    void setup() {
        log.debug("@BeforeEach");
    }

    @Test
    void test1() {
        log.debug("@Test [1]");
    }

    @Test
    void test2() {
        log.debug("@Test [2]");
    }

    @AfterEach
    void teardown() {
        log.debug("@AfterEach");
    }

    @AfterAll
    static void teardownAll() {
        log.debug("@AfterAll");
    }

    @Nested
    class Nested1 {
        private final Logger log = LoggerFactory.getLogger(Nested2.class);

        public Nested1() {
            log.debug("Constructor");
        }

        @BeforeEach
        void setup() {
            log.debug("@BeforeEach");
        }

        @Test
        void test1() {
            log.debug("@Test [1]");
        }

        @Test
        void test2() {
            log.debug("@Test [2]");
        }

        @AfterEach
        void teardown() {
            log.debug("@AfterEach");
        }
    }

    @Nested
    class Nested2 {
        private final Logger log = LoggerFactory.getLogger(Nested2.class);

        public Nested2() {
            log.debug("Constructor");
        }

        @BeforeEach
        void setup() {
            log.debug("@BeforeEach");
        }

        @Test
        void test1() {
            log.debug("@Test [1]");
        }

        @Test
        void test2() {
            log.debug("@Test [2]");
        }

        @AfterEach
        void teardown() {
            log.debug("@AfterEach");
        }
    }
}
