package de.cofinpro.training.softwareengineering;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;

/**
 * Entnommen von
 * <a href="https://github.com/bonigarcia/mastering-junit5/blob/master/junit5-basic-tests/src/test/java/io/github/bonigarcia/LifecycleTest.java">https://github.com/bonigarcia/mastering-junit5/blob/master/junit5-basic-tests/src/test/java/io/github/bonigarcia/LifecycleTest.java</a>
 */
@Slf4j
class LifecycleTest {

    @BeforeAll
    static void setupAll() {
        log.debug("@BeforeAll");
    }

    @BeforeEach
    void setup() {
        log.debug("@BeforeEach");
    }

    @Test
    void test1() {
        log.debug("@Test [1]");
    }

    @Test
    void test2() {
        log.debug("@Test [2]");
    }

    @AfterEach
    void teardown() {
        log.debug("@AfterEach");
    }

    @AfterAll
    static void teardownAll() {
        log.debug("@AfterAll");
    }
}
