package de.cofinpro.training.softwareengineering;

/**
 * Bietet Funktionalität rund um User an.
 */
public interface UserService {
    User getCurrentUser();

    /**
     * Protokolliert eine Aktion.
     *
     * @param user
     * @param action
     */
    void protocolAction(User user, Action action);
}
