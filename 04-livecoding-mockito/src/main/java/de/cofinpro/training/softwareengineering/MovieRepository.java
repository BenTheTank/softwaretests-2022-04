package de.cofinpro.training.softwareengineering;

/**
 * Verantwortlich für den Datenzugriff auf {@link Movie}-Objekte.
 */
public class MovieRepository {
    public Movie getMovieWithTitle(String title) {
        return new Movie(title);
    }
}
