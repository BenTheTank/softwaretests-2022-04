package de.cofinpro.training.softwareengineering;

import lombok.Value;

/**
 * Repräsentiert einen Benutzer.
 */
@Value
public class User {
    int id;
    String username;
}
