package de.cofinpro.training.softwareengineering;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static java.util.Collections.unmodifiableList;

/**
 * Repräsentiert einen Film mit seinen Bewertungen.
 */
public class Movie {
    private final String title;
    static List<Integer> ratings = new ArrayList<>();

    public Movie(String title) {
        this.title = title;
    }

    public void addRating(RatingService ratingService, int rating) {
        ratingService.addRating(this, rating);
    }

    public void addRatings(RatingService ratingService, int... ratings) {
        ratingService.addRatings(this, ratings);
    }

    public List<Integer> getRatings() {
        return unmodifiableList(ratings);
    }

    public String getTitle() {
        return title;
    }

    public double getAverageRating() throws NoRatingsException {
        if (ratings.isEmpty()) {
            throw new NoRatingsException();
        }
        return ratings.stream().reduce(0, Integer::sum) / (double) ratings.size();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Movie movie = (Movie) o;
        return Objects.equals(title, movie.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title);
    }
}
