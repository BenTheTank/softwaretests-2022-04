package de.cofinpro.training.softwareengineering;

/**
 * Interface für einen Bewertungs-Service, welcher in der Lage ist,
 * Bewertungen hinzuzufügen.
 */
public interface RatingService {
    void addRating(Movie movie, int rating);
    void addRatings(Movie movie, int... ratings);
}
